**THIS REPO IS FOR THE FORGE 1.12.2 VERSION**\
**FABRIC VERSION HERE: https://gitlab.com/vekwrite/quickturnmod**\
**1.18.2 FORGE VERSION HERE: https://gitlab.com/endode/quick-turn-mod-forge-1.18.2**

The **Fabric** version of this mod is built for 1.17.x, but works perfectly with newer versions (1.19.x, 1.20.x). This version requires the Fabric API\
The **Forge** version of the mod supports 1.12.2 and 1.18.2

Pressing the quick turn key (default is 'r') will make the player spin 180 degrees. This feature is inspired from Mirror's Edge and Mirror's Edge Catalyst, where you can press 'q' to spin around. You can configure the key in the controls menu like any other key.

![Quick Turn Configurable Key in Controls Menu](https://gitlab.com/vekwrite/quickturnmod/-/raw/main/quickturn_configurable_key_controls.jpg)

![Quick Turn Mod Demonstration](https://gitlab.com/vekwrite/quickturnmod/-/raw/main/quickturn_demo.gif)

[Modrinth Page](https://modrinth.com/mod/quick-turn-mod)\
[CurseForge Page](https://www.curseforge.com/minecraft/mc-mods/quick-turn-mod)

[Fabric Version Source](https://gitlab.com/vekwrite/quickturnmod)\
[Forge 1.12.2 Version Source](https://gitlab.com/vekwrite/quickturnmod-forge)\
[Forge 1.18.2 Version Source](https://gitlab.com/endode/quick-turn-mod-forge-1.18.2)

### Q/A:
- Is the mod open source?
- ***Yes, all versions of the mod are open source***
- ***You can find the source for the Fabric version here: https://gitlab.com/vekwrite/quickturnmod***
- ***You can find the source for the Forge 1.12.2 version here: https://gitlab.com/vekwrite/quickturnmod-forge***
- ***You can find the source for the Forge 1.18.2 version here: https://gitlab.com/endode/quick-turn-mod-forge-1.18.2***
- Will you continue to update the mod?
- ***(For the Fabric version) If it becomes incompatible with the latest version of Fabric/Fabric API then I'll fix it***
- Where can I contact you?
- ***Somewhere on the website http://endode.gitlab.io/***
- Will there be a Quilt version?
- ***At the moment, it works perfectly with Quilt, so there is no need for a special Quilt version***
- Will there be a release for modern Forge versions?
- ***I have tested it with Sinytra Connector and it appears to work perfectly, although you'll need Connector Extras for the config menu in the Fabric version. Maybe, if someone asks for it then maybe, wait, someone has asked, oh, maybe I'll look into it at some point. I don't use modern Forge versions very often myself, that's why there isn't any versions for that yet***
- Will you port it to older Minecraft versions?
- ***If enough people want it, I'll look into it***
- Is this mod incompatible with any other mods?
- ***No, it's really simple and doesn't do anything complex at all, so there shouldn't be any incompatibilities***
- Can I add this to a modpack?
- ***Of course!***
