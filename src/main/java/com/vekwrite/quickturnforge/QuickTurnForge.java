package com.vekwrite.quickturnforge;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.Logger;

@Mod(modid = QuickTurnForge.MODID, name = QuickTurnForge.NAME, version = QuickTurnForge.VERSION)
public class QuickTurnForge {
    public static final String MODID = "vekquickturn";
    public static final String NAME = "Quick Turn Mod";
    public static final String VERSION = "1.0";

    private static Logger logger;

    public static KeyBinding quickturn_key = new KeyBinding("key.vekquickturn.quickturn", 19, "category.vekquickturn.keycat");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        if(event.getSide() == Side.SERVER) {
            logger.warn(NAME + " is meant to be ran on the client only, it is not meant to be ran on the server itself");
        }
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        logger.info(NAME + " init()");
        ClientRegistry.registerKeyBinding(quickturn_key);
        MinecraftForge.EVENT_BUS.register(new QuickTurnHandler());
    }

}
