package com.vekwrite.quickturnforge;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class QuickTurnHandler {

   @SideOnly(Side.CLIENT)
   @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
       if(QuickTurnForge.quickturn_key.isPressed()) {
           EntityPlayerSP player = FMLClientHandler.instance().getClientPlayerEntity();
           player.rotationYaw += 180;
       }
   }

}
